package org.example.code.classAbstract;

import org.example.code.interfaces.IElectronico;
import org.example.code.interfaces.IProducto;

public abstract class Electronico extends Producto implements IElectronico {

    private String fabricante;

    public Electronico(double precio, String fabricante) {
        super(precio);
        this.fabricante = fabricante;
    }

    @Override
    public String getFabricante() {
        return fabricante;
    }

    public void setFabricante(String fabricante) {
        this.fabricante = fabricante;
    }

    @Override
    public String toString() {

       StringBuilder sb = new StringBuilder();
       sb.append(super.toString());
       sb.append("Fabricante: " + this.fabricante);

        return sb.toString();
    }
}

