package org.example.code.classAbstract;

import org.example.code.interfaces.IProducto;

public abstract class Producto implements IProducto {

    private double precio;

    public Producto(double precio) {
        this.precio = precio;
    }

    @Override
    public double getPrecio() {
        return precio;
    }


    public void setPrecio(double precio) {
        this.precio = precio;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Precio: " + precio + "\n");

        return sb.toString();
    }
}
