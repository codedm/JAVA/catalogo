package org.example.code.model;

import org.example.code.classAbstract.Electronico;


public class TvLcd extends Electronico {

    private int pulgada;

    public TvLcd(double precio, String fabricante, int pulgada) {
        super(precio, fabricante);
        this.pulgada = pulgada;
    }

    public int getPulgada() {
        return pulgada;
    }

    public void setPulgada(int pulgada) {
        this.pulgada = pulgada;
    }




    @Override
    public double getPrecioVenta() {
        return getPrecio();
    }

    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder("\n ---------TV LCD ----------\n");
        sb.append(super.toString());
        sb.append("\nPulgada: " + pulgada);
        sb.append("\n");
        return sb.toString();
    }
}
