package org.example.code.model;

import java.util.Date;

public class Comic extends Libro {

    private String personaje;


    public Comic(double precio, String editorial, String autor, String titulo, String personaje) {
        super(precio, editorial, autor, titulo);
        this.personaje = personaje;
    }

    public String getPersonaje() {
        return personaje;
    }

    public void setPersonaje(String personaje) {
        this.personaje = personaje;
    }



    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder();
        sb.append("Comic");
        sb.append(super.toString());
        sb.append("\nPersonaje: " + personaje);

        return sb.toString();
    }
}
