package org.example.code.model;

import org.example.code.classAbstract.Electronico;


public class IPhone extends Electronico  {


    private String color;
    private String modelo;

    public IPhone(double precio, String fabricante, String color, String modelo) {
        super(precio, fabricante);
        this.color = color;
        this.modelo = modelo;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }


    @Override
    public double getPrecioVenta() {
        return getPrecio();
    }

    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder("\n--------------PHONE--------------\n");
        sb.append(super.toString());
        sb.append("\nColor: " + color);
        sb.append("\nModelo: " + modelo);


        return sb.toString();
    }
}
