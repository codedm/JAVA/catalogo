package org.example.code.model;

import org.example.code.classAbstract.Producto;
import org.example.code.interfaces.ILibro;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Libro extends Producto implements ILibro {

   private Date fechaPublicacion ;
   private String titulo;
   private String autor;
   private String editorial;


   public Libro(double precio, String editorial, String autor, String titulo) {
        super(precio);
        this.editorial = editorial;
        this.autor = autor;
        this.titulo = titulo;
        this.fechaPublicacion = new Date();


   }


    @Override
    public Date getFechaPublicacion() {

       return fechaPublicacion;
    }

    @Override
    public String getAutor() {
        return autor;
    }

    @Override
    public String getTitulo() {
        return titulo;
    }

    @Override
    public String getEditorial() {
        return editorial;
    }

    @Override
    public double getPrecioVenta() {
        return getPrecio();
    }


    @Override
    public String toString() {
       StringBuilder sb = new StringBuilder("-------LIBRO----------\n");
       sb.append(super.toString());
       sb.append("Titulo: " + titulo);
       sb.append("\nAutor: " + autor);
       sb.append("\nEditorial: " + editorial);
       sb.append("\n ");


        return sb.toString();
    }
}
