package org.example.code;

import org.example.code.classAbstract.Producto;
import org.example.code.interfaces.IProducto;
import org.example.code.model.Comic;
import org.example.code.model.IPhone;
import org.example.code.model.Libro;
import org.example.code.model.TvLcd;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        IProducto[] productos = new IProducto[2];




        Libro superman = new Libro(
                1200,
                "DC-Editorial",
                " Jerry Siegel ",
                "Tierra X");

        TvLcd tv = new TvLcd(
                3400,
                "LG",
                40);

        TvLcd tv2 = new TvLcd(
                4000,
                "Samsung",
                60 );

        IPhone phone = new IPhone(
                400,
                "Apple",
                "White",
                "IO14"
        );


        List<Producto> listaProductos =  Arrays.asList(superman, tv, tv2, phone);

        for( Producto producto : listaProductos ){
            System.out.println(producto);
        }




   }
}